import glob
from typing import List
import os
import base64
import json
import requests
import argparse

YA_TOKEN = "CggVAgAAABoBMxKABCtHaz5nCUDPe2ybb8xejFqChbANswsfOUASft4F_Q1JCseVxAepb8NNtqB1i4fBzyd782SSrWYofDfOuKQ7SgpfeSSZI0V6wDYLgjoVZxsgzyr02Mymfw8TqrCtvXE1NAJ27j6gA_ko54yFLcaVD8crskFhrtekrj_Gm4S8Q2PY075I3tAmKnEpKbzo8Isr5_xTuInEgjg_E6kppQh31vdA2wXbc41IW0-C4W4LO6kMtHlVIyQYP3t9NVmDQcAw1fvhmEk3JjgqzJsitNFZZmZU4HJgsUTSLCm-hkSXE_610gL8Yi-9amLJFAOk3wsra34sTWZ2HAQRy42pHGhpNU18g_rm3DQuJRoLg7zgXJa3CXCqt48W5dMYnW6Hie3hCAewfjqkvB11ps-3TV-byadLIlg9dqB6w6G8k0HzjiizjLGZ7Wvod6yxDBV6BZT6BWraY4rzz1LcvrEAX1yl4hF7JlFSBGmNmFajfgjqJ_GiptZX9mGAJTGZRPuazvJhZtFEBO_eUBo4WwYXn0wq9VCvTBzbEfbMlKphX-g2-42us7Ma_VyL8WkZp-YZ4HqEoZ_mLMWt-88FKxCUvFnGI9mESVQ3aAUU_d9JApFYl4ZTv-GGuHLcqanB4Bb9ItVfYKjU3E-o40mKdW0-z2cexzOy38gDQQ8oQYRzXXv4FE8UGiQQh-r29AUYx7v59AUiFgoUYWplMzRuajBmN2MxYXQ0cWY4dHE= "

HEADER = {
    "Content-Type": "application/json",
    "authorization": 'Bearer ' + YA_TOKEN,
}


def find_files(path: str, files: List):
    files_paths_array = []
    for file_ext in files:
        for x in os.walk(path):
            for y in glob.glob(os.path.join(x[0], f'*.{file_ext}')):
                files_paths_array.append(y)
    return files_paths_array


def get_yandex_cloud_folder_id():
    my_cloud = requests.get('https://resource-manager.api.cloud.yandex.net/resource-manager/v1/clouds',
                            headers=HEADER).json()['clouds'][0]['id']

    my_folder = \
    requests.get(f'https://resource-manager.api.cloud.yandex.net/resource-manager/v1/folders?cloudId={my_cloud}',
                 headers=HEADER).json()['folders'][0]['id']
    return my_folder


def encode_file(file_name):
    file = open(file_name, 'rb')
    file_content = file.read()
    return base64.b64encode(file_content)


def post_to_yandex_vision_API(encoded_file, folder_id):
    body_dict = {
        "folderId": folder_id,
        "analyze_specs": [{
            "content": encoded_file.decode('ascii'),
            "features": [{
                "type": "TEXT_DETECTION",
                "text_detection_config": {
                    "language_codes": ["*"]
                }
            }]
        }]
    }
    r = requests.post(r'https://vision.api.cloud.yandex.net/vision/v1/batchAnalyze', headers=HEADER,
                      data=json.dumps(body_dict)).json()
    return r


def parse_yandex_vison_output_for_file(vision_dict):
    blocks = vision_dict['results'][0]['results'][0]['textDetection']['pages'][0]['blocks']
    result_text = []
    for block in blocks:
        block_lines = block['lines']
        for line in block_lines:
            block_words = line['words']
            for word in block_words:
                result_text.append(word['text'])
    return ' '.join(result_text)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', dest='file_folder_path')
    args = parser.parse_args()

    pictures_text = []

    file_list = find_files(path=args.file_folder_path, files=['png', 'jpeg'])
    folder_id = get_yandex_cloud_folder_id()
    for file in file_list:
        encoded_data = encode_file(file)
        vision_responce = post_to_yandex_vision_API(encoded_file=encoded_data, folder_id=folder_id)
        extracted_text = parse_yandex_vison_output_for_file(vision_responce)
        pictures_text.append((file, extracted_text))

    with open(args.file_folder_path + '/extracted_text_file.txt', 'w') as f:
        for picture in pictures_text:
            f.write(picture[0] + '\n')
            f.write(picture[1] + '\n')
            f.write('---------------------------------------\n')

    print('All text from pictures was written to file extracted_text_file.txt')
