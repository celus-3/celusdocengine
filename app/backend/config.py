import yaml
import os
from dotenv import load_dotenv
load_dotenv()
FILE_STORAGE_PATH = os.path.normpath(os.environ["FILE_STORAGE_PATH"])

local_path = os.path.dirname(os.path.abspath(__file__))

def build_storage_path(filename, root_dir=FILE_STORAGE_PATH):
    return os.path.join(root_dir, filename)

# ya vision config
stand_configs_path = os.path.join(local_path, 'stand_configs.yml')
YA_VISION_CONFIG = yaml.load(open(stand_configs_path, 'r'), Loader=yaml.FullLoader)
YA_VISION_KEY = YA_VISION_CONFIG['yandex_cloud']['YA_TOKEN']

#file storage config
INCOMING_DOCUMENTS_FOLDER_PATH = build_storage_path("pdf_storage")
RESULT_DOCUMENT_PATH = build_storage_path("result_documents")
if not os.path.exists(INCOMING_DOCUMENTS_FOLDER_PATH): os.makedirs(INCOMING_DOCUMENTS_FOLDER_PATH)
if not os.path.exists(RESULT_DOCUMENT_PATH): os.makedirs(RESULT_DOCUMENT_PATH)

#mongo database configs
DB_CONFIG = yaml.load(open(stand_configs_path, 'r'), Loader=yaml.FullLoader)

MONGO_DB_HOST = DB_CONFIG['mongodb']['host']
MONGO_DB_PORT = DB_CONFIG['mongodb']['port']
MONGO_CELUS_DB = DB_CONFIG['mongodb']['db_name']
MONGO_CELUS_COLLECTION = DB_CONFIG['mongodb']['collection']

# insert docs fields
DOCUMENT_PAGE_NUMBER_FIELD = 'page_number'
DOCUMENT_SPECIAL_ELEMENTS_FIELD = 'special_elements'
DOCUMENT_TEXT_FIELD = 'text'


# Search configs
SPECIAL_ELEMENTS = ['Figure', 'Table']
TEXT_INDEX_FIELD = 'picture_info.text'

PICTURE_EXTENSION = 'jpeg'
DOCUMENT_EXTENSION = 'pdf'

PDF_FILE_NAME = 'pdf_id'
PDF_FILE_URL = 'pdf_url'
PICTURE_FILE_NAME = 'picture_id'
PICTURE_URL = 'picture_url'
PICTURE_CONTENT_NAME = 'picture_info'
DEFAULT_PDF_PARAMS = {
    'x': 0,
    'y': 0,
    'w': 210,
    'h': 297
}

RESULT_PDF_URL = 'result_pdf_url'
RESULT_FULL_PDF_URLS = 'full_pdf_urls'
RESULT_TEXT = 'text'
RESULT_PICTURES_URLS = 'result_pictures_urls'




