from flask import Blueprint, Flask
from flask_restful import Api
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint

from src.controllers.add_document import AddDocument
from src.controllers.list_documents import ListDocuments
from src.controllers.search_documents import SearchDocument

# config
app = Flask(__name__)
api_bp = Blueprint('api', __name__)
api = Api(api_bp)
app.register_blueprint(api_bp, url_prefix='/api')
CORS(app, resources={r"/*": {"origins": "*"}}, headers=['Content-Type'], expose_headers=['Access-Control-Allow-Origin'],
     supports_credentials=True)

# swagger
SWAGGER_URL = '/swagger'  # URL for exposing Swagger UI (without trailing '/')
API_URL = '/static/swagger.json'
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "Celus 3"
    },
)

# Register blueprint at URL
# (URL must match the one given to factory function above)
app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)



# Routes
api.add_resource(ListDocuments, '/docs')
api.add_resource(AddDocument, '/add')
api.add_resource(SearchDocument, '/search')


@app.route('/', methods=['GET'])
def check_backend():
    return "works"


if __name__ == '__main__':
    # host = '130.193.36.242'
    host = '0.0.0.0'

    # port = '3000'
    # app.run(host=host, port=port)
    app.run(host=host)

# python -m flask run --host=0.0.0.0
