import re
import config as app_config


class Parser:
    def __init__(self):
        self._special_elements = app_config.SPECIAL_ELEMENTS

    def _check_for_specials(self, text):
        special_elements = set()
        for e in self._special_elements:
            result = re.findall(r'{} \d.'.format(e), text)
            if len(result) > 0:
                special_elements.add(e)
        return list(special_elements)

    def _get_text_from_page(self, page):
        line_text = []
        blocks = len(page['blocks'])
        for block_id in range(blocks):
            lines = page['blocks'][block_id]['lines']
            for line in lines:
                line_words = line['words']
                for word in line_words:
                    line_text.append(word['text'])
        return ' '.join(line_text)

    def _get_page(self, payload):
        return payload['results'][0]['results'][0]['textDetection']['pages'][0]

    def _form_page_payload(self, payload, page_number):
        result = {app_config.DOCUMENT_PAGE_NUMBER_FIELD: page_number,
                  app_config.DOCUMENT_SPECIAL_ELEMENTS_FIELD: set(),
                  app_config.DOCUMENT_TEXT_FIELD: None}
        page = self._get_page(payload)
        text = self._get_text_from_page(page)
        elements = self._check_for_specials(text)
        result[app_config.DOCUMENT_SPECIAL_ELEMENTS_FIELD] = elements
        result[app_config.DOCUMENT_TEXT_FIELD] = text
        return result





