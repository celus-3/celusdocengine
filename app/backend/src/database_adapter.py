import config as app_config
import pymongo
from fpdf import FPDF
from src.bucket import Bucket
import os
import logging


class DatabaseAdapter:
    def __init__(self):
        self._mongo_client = pymongo.MongoClient(host=app_config.MONGO_DB_HOST, port=int(app_config.MONGO_DB_PORT))
        self._mongo_db = self._mongo_client[app_config.MONGO_CELUS_DB]
        self._mongo_doc_collection = self._mongo_db[app_config.MONGO_CELUS_COLLECTION]
        self._mongo_doc_collection.create_index([(app_config.TEXT_INDEX_FIELD, "text")])
        self._logger = logging.getLogger(__name__)
        self._bucket = Bucket()

    def upload(self, d: dict):
        result = self._mongo_doc_collection.insert_one(d)
        return result.inserted_id

    def search(self, query: str, keywords, docs_id_list) -> dict:
        if len(keywords) == 0:
            keywords = app_config.SPECIAL_ELEMENTS
        if len(docs_id_list) == 0:
            result_docs = list(self._mongo_doc_collection.find({
                "picture_info.special_elements": {"$in": keywords},
                "$text": {"$search": query}
            },
                {'score': {'$meta': 'textScore'}}).sort([('score', {'$meta': 'textScore'})]))
        else:
            result_docs = list(self._mongo_doc_collection.find({
                "picture_info.special_elements": {"$in": keywords},
                "pdf_id": {"$in": docs_id_list},
                "$text": {"$search": query},
            },
                {'score': {'$meta': 'textScore'}}).sort([('score', {'$meta': 'textScore'})]))
        pdf = FPDF()
        result = {app_config.RESULT_PDF_URL: None, app_config.RESULT_FULL_PDF_URLS: [],
                  app_config.RESULT_TEXT: ' ', app_config.RESULT_PICTURES_URLS: []}
        if len(result_docs) > 0:
            for doc in result_docs:
                if doc[app_config.PDF_FILE_URL] not in result[app_config.RESULT_FULL_PDF_URLS]:
                    result[app_config.RESULT_FULL_PDF_URLS].append(doc[app_config.PDF_FILE_URL])

                if doc[app_config.PICTURE_URL] not in result[app_config.RESULT_PICTURES_URLS]:
                    result[app_config.RESULT_PICTURES_URLS].append(doc[app_config.PICTURE_URL])

                result[app_config.RESULT_TEXT] += doc[app_config.PICTURE_CONTENT_NAME][app_config.DOCUMENT_TEXT_FIELD]

                suffix = f'.{app_config.PICTURE_EXTENSION}'
                folder_path = os.path.join(app_config.FILE_STORAGE_PATH, doc[app_config.PDF_FILE_NAME].split('.')[0])
                picture_path = os.path.join(folder_path, str(doc[app_config.PICTURE_FILE_NAME]) + suffix)
                pdf.add_page()
                pdf.image(picture_path, x=app_config.DEFAULT_PDF_PARAMS['x'], y=app_config.DEFAULT_PDF_PARAMS['y'],
                          w=app_config.DEFAULT_PDF_PARAMS['w'], h=app_config.DEFAULT_PDF_PARAMS['h'])

            result_doc_path = os.path.join(app_config.RESULT_DOCUMENT_PATH,
                                           str(hash(pdf)) + f'.{app_config.DOCUMENT_EXTENSION}')
            pdf.output(result_doc_path, "F")
            with open(result_doc_path, 'rb') as fileobj:
                result_pdf_url = self._bucket.upload_file(file=fileobj, filename=str(hash(pdf)))
            result[app_config.RESULT_PDF_URL] = result_pdf_url
            # self._logger.info(f"Search result was saved to {result_doc_path}")
        return result
