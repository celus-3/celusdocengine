from src.database_adapter import DatabaseAdapter
from src.doc_uploader import DocUploader


class CelusApiHandler:
    def __init__(self):
        self._db_adapter = DatabaseAdapter()
        self._doc_uploader = DocUploader()

    def search(self, query, keywords=None, documents=None) -> dict:
        result = self._db_adapter.search(query, keywords, documents)
        return result

    def add_document(self, bytes_doc, doc_url, filename):
        upload_doc = self._doc_uploader._upload_document_to_mongo(bytes_doc, doc_url, filename)
        return upload_doc
