from flask import request

from src.controllers.base import Base
from src.celus_api_handler import CelusApiHandler


class SearchDocument(Base):
    def __init__(self):
        super().__init__()
        self._api_handler = CelusApiHandler()

    def post(self):
        docs_id_list = request.json['docs_id_list']
        keywords = request.json['keywords']
        query = request.json['query']
        print(query, keywords, docs_id_list)
        res = self._api_handler.search(query, keywords, docs_id_list)
        print(res)
        # res = {'new_pdf': "https://storage.yandexcloud.net/celus-3/stm32-new.pdf", 'full_pdf': ['https://storage.yandexcloud.net/celus-3/stm32.pdf']}
        return res
