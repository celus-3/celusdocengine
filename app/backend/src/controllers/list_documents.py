from src.controllers.base import Base


class ListDocuments(Base):

    def get(self):
        db = self.start_connection()
        docs_collection = db['celus_docs']
        docs = list(docs_collection.distinct('pdf_id'))

        print("get", docs)
        self.close_connection()
        return docs
