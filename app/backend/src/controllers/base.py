from flask_restful import Resource
import os
from pymongo import MongoClient

from dotenv import load_dotenv


class Base(Resource):
    def __init__(self):
        load_dotenv()
        self.DB_NAME = os.environ['DB_NAME']
        self.DB_USER = os.environ['DB_USER']
        self.DB_PASS = os.environ['DB_PASS']
        self.DB_HOST = os.environ['DB_HOST']
        self.DB_PORT = int(os.environ['DB_PORT'])
        self.client = MongoClient(self.DB_HOST, self.DB_PORT)

    def start_connection(self):
        try:
            db = self.client[self.DB_NAME]
            print("started connection with Db:", db)
            return db
        except Exception as e:
            print("failed connection", e)

    def close_connection(self):
        self.client.close()
        print("close connection")
        # try:
        #     self.connection.commit()
        #     self.cursor.close()
        #     self.connection.close()
        # except Exception as e:
        #     print("error close connect: ", e)

