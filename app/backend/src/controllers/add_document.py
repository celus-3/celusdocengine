from flask import request
import os
from flask_restful import Resource
from src.bucket import Bucket
from src.celus_api_handler import CelusApiHandler


class AddDocument(Resource):
    def __init__(self):
        super().__init__()
        self.api_handler = CelusApiHandler()
        self.bucket = Bucket()

    def post(self):
        filename = request.form['name']
        file = request.files['doc']
        filepath = self.save_file(file, filename)
        file_url = self.bucket.upload_from_file(filepath, filename)
        # filename = os.path.basename(filepath)
        print("start api_handler.add_document")
        self.api_handler.add_document(bytes_doc=open(filepath, 'rb').read(), doc_url=file_url, filename=filename)
        print("finish api_handler.add_document")
        return {'res': 'ok'}

    def save_file(self, file, filename):
        path = os.environ['FILE_STORAGE_PATH'] + '/pdf_storage'
        fullpath = os.path.join(path, filename)
        file.save(fullpath)
        print(filename, "saved", fullpath)
        return fullpath
