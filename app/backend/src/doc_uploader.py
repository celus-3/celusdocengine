import pdf2image
from src.bucket import Bucket
from src import utils
import config as app_config
from src.parser import Parser
from src.database_adapter import DatabaseAdapter
import os
import logging


class DocUploader:
    def __init__(self):
        self._folder_id = utils.get_folder_id()
        self._parser = Parser()
        self._db_adapter = DatabaseAdapter()
        self._logger = logging.getLogger(__name__)
        self._bucket = Bucket()

    def _pdf2jpegs(self, file_obj: str, folder_path: str):
        pictures_from_pdf_paths = pdf2image.convert_from_bytes(pdf_file=file_obj, dpi=200,
                                                              output_folder=folder_path,
                                                              fmt='jpeg',
                                                              jpegopt={"quality": 10, "progressive": False,
                                                                       "optimize": True},
                                                              thread_count=-1, grayscale=True, paths_only=True)
        self._logger.debug(f"Pictures were extracted from PDF!")
        return pictures_from_pdf_paths

    def _form_picture_payload(self, picture_path: str, pdf_name: str, doc_url: str):
        encoded_picture = utils.encode_file(picture_path)
        picture_id_hash = hash(encoded_picture)
        # get picture location dir
        picture_location = os.path.dirname(picture_path)
        # we store a a picture by a hash in a folder named by pdf hash
        page_number = int(picture_path.split('-')[-1].split('.')[0])
        import mimetypes
        print("mem", mimetypes.MimeTypes().guess_type(picture_path)[0])
        with open(picture_path, 'rb') as fileobj:
            picture_url = self._bucket.upload_pic(fileobj, f"{pdf_name}/{picture_id_hash}.jpeg")
        os.rename(picture_path, os.path.join(picture_location, f'{picture_id_hash}.jpeg'))
        ya_vision_responce = utils.post_to_yandex_vision_API(encoded_picture, self._folder_id)
        picture_content = self._parser._form_page_payload(ya_vision_responce, page_number)
        picture_payload = {app_config.PICTURE_FILE_NAME: str(picture_id_hash),
                           app_config.PICTURE_URL: picture_url,
                           app_config.PDF_FILE_NAME: pdf_name,
                           app_config.PDF_FILE_URL: doc_url,
                           app_config.PICTURE_CONTENT_NAME: picture_content}
        return picture_payload

    def _upload_document_to_mongo(self, bytes_doc: str, doc_url, pdf_file_name):
        #получаем url pdf
        pdf_doc_folder_path = os.path.join(app_config.FILE_STORAGE_PATH, pdf_file_name.split('.')[0])
        # for new doc we create a folder by a hash of pdf doc
        os.mkdir(pdf_doc_folder_path)
        # store pictures of pdf in pdf folder
        pdf_pictures_paths = self._pdf2jpegs(bytes_doc, pdf_doc_folder_path)
        pdf_doc_folder_name = os.path.basename(pdf_doc_folder_path)
        for picture_path in pdf_pictures_paths:
            picture_payload = self._form_picture_payload(picture_path, pdf_doc_folder_name, doc_url)
            self._db_adapter.upload(picture_payload)
        self._logger.info("Doc was ploaded to database")
        return True
