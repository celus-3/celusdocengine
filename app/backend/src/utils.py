import config as app_config
import requests
import base64
import json
import os
import uuid

headers = {
        "Content-Type": "application/json",
        "authorization": 'Bearer ' + app_config.YA_VISION_CONFIG['yandex_cloud']['YA_TOKEN'],
    }

def get_folder_id():
    ya_vision_url = 'https://resource-manager.api.cloud.yandex.net/resource-manager/v1/'
    my_cloud = requests.get(ya_vision_url + 'clouds', headers=headers).json()['clouds'][0]['id']
    my_folder = requests.get(ya_vision_url + f'folders?cloudId={my_cloud}', headers=headers).json()['folders'][0]['id']
    return my_folder

def encode_file(file_name):
    file = open(file_name, 'rb')
    file_content = file.read()
    return base64.b64encode(file_content)

def post_to_yandex_vision_API(encoded_file, folder_id):
    body_dict = {
        "folderId": folder_id,
        "analyze_specs": [{
            "content": encoded_file.decode('ascii'),
            "features": [{
                "type": "TEXT_DETECTION",
                "text_detection_config": {
                    "language_codes": ["*"]
                }
            }]
        }]
    }
    r = requests.post(r'https://vision.api.cloud.yandex.net/vision/v1/batchAnalyze', headers=headers, data=json.dumps(body_dict)).json()
    return r

def get_filename_from_path(path):
    return os.path.basename(path)

def get_unique_filename():
    filename = str(uuid.uuid4())
    return filename