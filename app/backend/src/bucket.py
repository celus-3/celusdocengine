from dotenv import load_dotenv
import boto3
from botocore.exceptions import ClientError
import os
import mimetypes

class Bucket:
    def __init__(self):
        load_dotenv()
        # service_name = os.environ['YA_SERVICE_NAME']
        self.url = os.environ['YA_BUCKET_URL']
        session = boto3.session.Session()
        self.s3 = session.client(
            aws_access_key_id=os.environ['aws_access_key_id'],
            aws_secret_access_key=os.environ['aws_secret_access_key'],
            region_name='ru-central1',
            service_name='s3',
            endpoint_url=self.url
        )
        self.bucket_name = 'celus-3'

    def get_file_bites(self, filename):
        get_object_response = self.s3.get_object(Bucket=self.bucket_name, Key=filename)
        file = get_object_response['Body'].read()
        return file

    def download_file(self, path, filename):
        with open(path, 'wb') as f:
            self.s3.download_fileobj(self.bucket_name, filename, f)

    def upload_file(self, file, filename):
        url = ""
        try:
            self.s3.upload_fileobj(file, self.bucket_name, filename,
                                   ExtraArgs={'ContentType': 'application/pdf', 'ACL': 'public-read'})
            url = "{}/{}/{}".format(self.url, self.bucket_name, filename)
        except ClientError as e:
            print(e)
        return url

    def upload_pic(self, file, filename):
        url = ""
        try:
            self.s3.upload_fileobj(file, self.bucket_name, filename,
                                   ExtraArgs={'ContentType': 'image/jpeg', 'ACL': 'public-read'})
            url = "{}/{}/{}".format(self.url, self.bucket_name, filename)
        except ClientError as e:
            print(e)
        return url

    def upload_from_file(self, file, filename):
        url = ""
        try:
            self.s3.upload_file(file, self.bucket_name, filename,
                                ExtraArgs={'ContentType': 'application/pdf', 'ACL': 'public-read'})
            url = "{}/{}/{}".format(self.url, self.bucket_name, filename)
        except ClientError as e:
            print(e)
        return url


