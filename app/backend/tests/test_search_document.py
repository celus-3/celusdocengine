import unittest
from src.bucket import Bucket
from src.celus_api_handler import CelusApiHandler


class SearchDocumentTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.bucket = Bucket()
        cls.api_handler = CelusApiHandler()

    def test_search_document(self):
        query = 'memory'
        keywords = []
        docs_id_list = []
        res = self.api_handler.search(query, keywords, docs_id_list)
        print(res)
