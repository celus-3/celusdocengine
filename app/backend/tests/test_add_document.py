import unittest
import os
from src.bucket import Bucket
from src.celus_api_handler import CelusApiHandler
import config as app_config

class AddDocumentTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.bucket = Bucket()
        cls.api_handler = CelusApiHandler()

    def test_add_document(self):
        filename = '01.pdf'
        input_file = os.path.join(app_config.INCOMING_DOCUMENTS_FOLDER_PATH, filename)
        with open(input_file, 'rb') as file_obj:
            file_url = self.bucket.upload_pic(file_obj, filename)
        result = self.api_handler.add_document(bytes_doc=open(input_file, 'rb').read(), doc_url=file_url, filename=filename)
        self.assertTrue(result)


