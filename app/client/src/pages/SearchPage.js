import React, {Component} from 'react';
import {
    Grid, Button, Paper
} from '@material-ui/core';
import {toast} from 'react-toastify';

import Result from "../components/SearchComponents/Result";
import ListDocs from "../components/SearchComponents/ListDocs";
import SearchingString from "../components/SearchComponents/SearchingString";
import ResultType from "../components/SearchComponents/ResultType";
import {searchService, getDocsService} from "../services/api";
import styles from './pages.module.css'

class SearchPage extends Component {

    state = {
        docs: [],
        result: {
            full_pdf_urls: [],
            result_pdf_url: '',
            text: '',
            result_pictures_urls: []
        },
        docs_id_list: [],
        keywords: [],
        query: "",
        isLoading: false,
    };

    componentDidMount = () => {
        this.getDocs()
    };

    getDocs = () => {
        getDocsService().then((res) => {
            console.log(res);
            this.setState({docs: res});
        })
            .catch((e) => {
                console.log("error getting docs", e);
                toast.error("error getting docs")
            })
    };

    getDocsIds = () => {
        let docs_id_list = this.state.docs_id_list;
        const isAll = docs_id_list.some((id) => id === 'all');
        if (isAll) {
            docs_id_list = []
        }
        return docs_id_list
    };

    sendData = () => {
        const docs_id_list = this.getDocsIds();
        const searchData = {
            docs_id_list: docs_id_list,
            keywords: this.state.keywords,
            query: this.state.query,
        };
        this.setState({isLoading: true});
        searchService(searchData)
            .then((res) => {
                // toast.success("success");
                console.log("search", res);
                this.setState({result: res});
            })
            .catch((e) => {
                toast.error("error");
                console.log(e);
            })
            .finally(() => this.setState({isLoading: false}));

    };

    handleOnSelect = (docs_id_list) => {
        this.setState({docs_id_list: docs_id_list});
    };

    handleOnQuery = (query) => {
        this.setState({query: query});
    };

    handleOnKeywords = (keywords) => {
        this.setState({keywords: keywords});
    };

    render() {
        return (
            <div>
                <Grid container justify="flex-start">
                    <Grid item md={3} style={{marginTop: "1em"}}>
                        <Paper>
                            <Grid item md={12} className={styles.gridItem} style={{margin: "1em"}}>
                                <ListDocs handleOnSelect={this.handleOnSelect} docs={this.state.docs}/>
                            </Grid>
                            <Grid item md={12} className={styles.gridItem}>
                                <SearchingString handleOnQuery={this.handleOnQuery}/>
                            </Grid>
                            <Grid item md={12} className={styles.gridItem} style={{margin: "1em"}}>
                                <ResultType handleOnKeywords={this.handleOnKeywords}/>
                            </Grid>
                            <Grid item md={12} className={styles.gridItem} style={{textAlign: "end"}}>
                                <Button variant="contained" color="primary" onClick={this.sendData}>Search</Button>
                            </Grid>
                        </Paper>
                    </Grid>
                    <Grid item md={9}>
                        <Result result={this.state.result} isLoading={this.state.isLoading}/>
                    </Grid>
                </Grid>

            </div>
        )
    }
}

export default SearchPage;