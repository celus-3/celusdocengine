import React, {Component} from 'react';
import {
    Grid
} from '@material-ui/core';
import UploadDoc from "../components/UploadComponents/UploadDoc";


class UploadPage extends Component {

    render() {
        return (
            <div>
                <Grid container justify="center">
                    <UploadDoc/>
                </Grid>
            </div>
        )
    }
}

export default UploadPage;