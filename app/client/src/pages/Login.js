import React, {Component} from 'react'
import {withRouter} from 'react-router-dom';

import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import {
    Button,
    Checkbox,
    Container,
    CssBaseline,
    FormControlLabel,
    Grid,
    TextField,
    Avatar,
    Link
} from '@material-ui/core'
import {toast} from 'react-toastify'

import styles from './pages.module.css'


class Login extends Component {

    state = {
        password: "",
        login: "",
    };

    signIn = (e) => {
        e.preventDefault();
        const {history} = this.props;
        const testData = "test";
        if (this.state.login === testData && this.state.password === testData) {
            toast.success("Success");
            history.push('/search');
        } else {
            toast.error("Login or password is incorrect");
        }
    };

    render() {
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline/>
                <div className={styles.paper}>
                    <Avatar className={styles.avatar}>
                        <LockOutlinedIcon/>
                    </Avatar>
                    <form noValidate>

                        <TextField
                            onChange={event => this.setState({login: event.target.value})}
                            value={this.state.login}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Login"
                            name="login"
                            autoComplete="login"
                            autoFocus
                            placeholder="test"
                            helperText="Login: test"
                        />
                        <TextField
                            onChange={event => this.setState({password: event.target.value})}
                            value={this.state.password}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            helperText="Password: test"
                            placeholder="test"
                        />
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary"/>}
                            label="Remember"
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={styles.submitBtn}
                            onClick={this.signIn}>
                            Login
                        </Button>
                        <Grid container>
                            <Grid item xs>
                                <Link href="#">
                                    Forgotten account?
                                </Link>
                            </Grid>
                            <Grid item>
                                <Link href="#">
                                    Sign Up
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        )
    }
}

export default withRouter(Login);