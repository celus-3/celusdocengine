import React from 'react';
import {Route, BrowserRouter as Router, Switch} from 'react-router-dom'
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Login from './pages/Login'
import SearchPage from './pages/SearchPage'
import AppBarComponent from './components/HeaderComponents/AppBarComponent'
import UploadPage from './pages/UploadPage'

toast.configure({
    position: "bottom-right",
    autoClose: 2000,
    hideProgressBar: true,
    closeOnClick: true,
});

function App() {
    return (
        <div className="App">
            <Router>
                <AppBarComponent/>
                <Switch>
                    <Route exact path="/">
                        <Login/>
                    </Route>
                    <Route exact path="/upload">
                        <UploadPage/>
                    </Route>
                    <Route exact path="/search">
                        <SearchPage/>
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;
