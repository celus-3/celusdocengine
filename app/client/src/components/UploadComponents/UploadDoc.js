import React, {Component} from 'react'
import {
    Button,
    TextField,
    Grid,
    FormLabel,
    FormControl,
    Paper
} from '@material-ui/core'
import CircularProgress from '@material-ui/core/CircularProgress';
import {toast} from 'react-toastify';
import styles from './upload.module.css';
import uploadPic from './uploadPic.jpg'

import {addDocService} from "../../services/api"

class UploadDoc extends Component {
    state = {
        fileInput: React.createRef(),
        name: "",
        isLoading: false,
    };

    handleChangeName = (event) => {
        this.setState({name: event.target.value});
    };

    makeFileForSend = (name) => {
        name.trim();
        name += '.pdf';
        const data = new FormData();
        const file = this.state.fileInput.current.files[0];
        data.append('doc', file);
        data.append("name", name);
        this.setState({name: ''});
        return data;
    };
    addDoc = (e) => {
        e.preventDefault();
        let name = this.state.name;
        if (!(name)) {
            toast.error("Doc name should be filled");
            return;
        }
        const data = this.makeFileForSend(name);
        this.setState({isLoading: true});
        addDocService(data)
            .then(() => {
                console.log("added doc");
                toast.success("Document is added");
            })
            .catch((e) => {
                console.log("error", e);
                toast.error("error")
            })
            .finally(() => this.setState({isLoading: false}));
    };

    render() {
        let progress;
        if (this.state.isLoading) {
            progress = <CircularProgress value={this.state.isLoading}/>
        } else {
            progress = <div></div>
        }
        return (
            <div>
                <Paper className={styles.paper}>

                    <FormControl className={styles.formControl} style={{width: "15em"}}>
                        <FormLabel id="upload-label" focused>Upload a File</FormLabel>
                        <div className={styles.imageUpload}>
                            <label htmlFor="file-input">
                                <img style={{maxWidth: "4em"}} src={uploadPic} alt=""/>
                            </label>

                            <input
                                accept="application/pdf"
                                id="file-input"
                                type="file"
                                ref={this.state.fileInput}
                            />
                        </div>

                        <TextField
                            required
                            id="outlined-basic"
                            label="Doc name"
                            value={this.state.name}
                            onChange={this.handleChangeName}
                            margin="normal"
                            fullWidth
                        />
                        <Button variant="contained" color="primary" onClick={this.addDoc}>Add doc</Button>
                    </FormControl>

                </Paper>

                <Grid container justify="center">
                    {progress}
                </Grid>
            </div>

        )
    }
}

export default UploadDoc;
