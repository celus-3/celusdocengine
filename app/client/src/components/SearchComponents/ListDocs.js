import React, {Component} from 'react'
import {
    FormControl,
    MenuItem,
    Select,
    Input,
    FormLabel
} from '@material-ui/core'
import styles from './search.module.css';

class ListDocs extends Component {

    state = {
        selectedDocsId: [],
    };

    handleChange = (event) => {
        this.setState({selectedDocsId: event.target.value});
        this.props.handleOnSelect(event.target.value);
    };

    render() {
        return (
            <div>
                <FormControl className={styles.formControl}>
                    <FormLabel id="select-label" focused required>List of docs</FormLabel>
                    <Select
                        labelId="select-label"
                        id="select-label"
                        multiple
                        value={this.state.selectedDocsId}
                        onChange={this.handleChange}
                        input={<Input/>}
                    >
                        <MenuItem value={'all'}>
                            All
                        </MenuItem>
                        {this.props.docs.map((doc) => (
                            <MenuItem key={doc} value={doc}>
                                {doc}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>

            </div>

        )
    }
}

export default ListDocs;
