import React, {Component} from 'react'
import {TextField, FormLabel, FormControl} from '@material-ui/core'
import styles from './search.module.css'

class SearchingString extends Component {
    state = {
        query: "",
    };

    handleChange = (event) => {
        const query = event.target.value;
        this.setState({query: query});
        this.props.handleOnQuery(query.trim());
    };

    render() {
        return (
            <div>
                <FormControl className={styles.formControl}>
                    <FormLabel id="keyWordsField" focused required>Keywords</FormLabel>
                    <TextField
                        id="keyWordsField"
                        multiline
                        variant="outlined"
                        fullWidth
                        placeholder="LQFP64"
                        value={this.state.query}
                        onChange={this.handleChange}
                    />
                </FormControl>
            </div>


        )
    }
}

export default SearchingString;
