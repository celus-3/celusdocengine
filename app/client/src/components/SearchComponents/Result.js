import React, {Component} from 'react'
import {Typography, Grid, Paper} from '@material-ui/core'
import 'react-pdf/dist/Page/AnnotationLayer.css';
import CircularProgress from "@material-ui/core/CircularProgress/CircularProgress";
import {Pagination} from '@material-ui/lab';


class Result extends Component {

    state = {
        pageNumber: 0,
    };

    changePage = (event, page) => {
        this.setState({pageNumber: page - 1})
    };

    render() {
        let result = this.props.result;
        let full_links;
        const full_pdfs = result.full_pdf_urls;
        if (full_pdfs.length) {
            full_links =
                <Grid item>
                    <span>
                        Full docs:&#160;&#160;
                        {full_pdfs.map((link) => {
                            const splitedLink = link.split('/');
                            return (
                                <span>
                                    <a target="_blank" rel="noopener noreferrer"
                                       href={link}>{splitedLink[splitedLink.length - 1]}</a>&#160;&#160;&#160;
                                    </span>
                            )
                        })}
                    </span>
                </Grid>
        }

        let resLink;
        if (result.result_pdf_url) {
            resLink = <Grid item>
                Result doc:&#160;&#160;
                <a target="_blank" rel="noopener noreferrer" style={{marginBottom: "2em"}}
                   href={result.result_pdf_url}>result pdf</a>
            </Grid>
        }

        return (
            <div>
                <Paper style={{minHeight: '50em'}}>

                    <Grid container justify="center">
                        <Typography variant="h6">
                            Output
                        </Typography>
                    </Grid>

                    <Grid container justify="center">
                        {this.props.isLoading ? <CircularProgress/> : ''}
                    </Grid>

                    <Grid container justify="space-around">
                        {full_links}
                        {resLink}
                    </Grid>

                    <Grid container justify="center">
                        {result.result_pictures_urls.length ?
                            <Pagination count={this.props.result.result_pictures_urls.length} color="primary"
                                        onChange={this.changePage}
                            /> : ''}
                    </Grid>

                    <Grid container justify="center">
                        <img src={this.props.result.result_pictures_urls[this.state.pageNumber]} width="700em" alt=""/>
                    </Grid>


                </Paper>
            </div>

        )
    }
}

export default Result;
