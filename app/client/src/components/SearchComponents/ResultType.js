import React, {Component} from 'react'
import {
    FormGroup,
    FormControlLabel,
    Checkbox, FormControl, FormLabel
} from '@material-ui/core'
import styles from './search.module.css';

class ResultType extends Component {

    state = {
        Table: false,
        Text: false,
        Figure: false,
    };

    handleChange = (event) => {
        const keywords = {...this.state, [event.target.name]: event.target.checked};
        this.setState(keywords);
        const keys = Object.keys(keywords);
        const res = keys.filter((key) => {
            if (key === "Text") {
                return false
            }
            return keywords[key]
        });
        this.props.handleOnKeywords(res);

    };

    render() {
        return (
            <div>

                <FormControl className={styles.formControl}>
                    <FormLabel id="result-type-label" focused required>Filter</FormLabel>

                    <FormGroup row>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="Table"
                                    checked={this.state.Table}
                                    onChange={this.handleChange}
                                />
                            }
                            label="Tables"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="Text"
                                    checked={this.state.text}
                                    onChange={this.handleChange}
                                />
                            }
                            label="Text"
                        />
                        <FormControlLabel
                            control={
                                <Checkbox
                                    name="Figure"
                                    checked={this.state.Figure}
                                    onChange={this.handleChange}
                                />
                            }
                            label="Figures"
                        />
                    </FormGroup>
                </FormControl>

            </div>


        )
    }
}

export default ResultType;
