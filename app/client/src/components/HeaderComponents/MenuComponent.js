import React from 'react';
import {Menu, MenuItem, Button} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import {withRouter} from 'react-router-dom';

class MenuComponent extends React.Component {
    state = {
        anchorEl: null,
    };

    handleClick = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleClose = () => {
        this.setState({anchorEl: null});
    };

    changePath = (path) => {
        this.handleClose();
        const {history} = this.props;
        history.push(path);
    };

    render() {
        const {anchorEl} = this.state;

        return (
            <div>
                <Button
                    aria-owns={anchorEl ? 'simple-menu' : undefined}
                    aria-haspopup="true"
                    onClick={this.handleClick}
                >
                    <MenuIcon/>
                </Button>
                <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    open={Boolean(anchorEl)}
                    onClose={this.handleClose}
                >
                    <MenuItem onClick={() => this.changePath("/upload")}>Upload</MenuItem>
                    <MenuItem onClick={() => this.changePath("/search")}>Search</MenuItem>
                </Menu>
            </div>
        );
    }
}

export default withRouter(MenuComponent);