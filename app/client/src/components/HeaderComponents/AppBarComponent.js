import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import {Toolbar, Typography, Button, IconButton} from '@material-ui/core';
import {Link} from 'react-router-dom'

import MenuComponent from './MenuComponent'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function AppBarComponent() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <MenuComponent/>
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>

                    </Typography>
                    <Button color="inherit" component={Link} to="/">Logout</Button>
                </Toolbar>
            </AppBar>
        </div>
    );
}