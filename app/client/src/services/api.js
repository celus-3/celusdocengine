import axios from 'axios';

const host = 'http://localhost:5000';
const defaultConfig = {
  mode: 'no-cors',
  headers: {
    'Access-Control-Allow-Origin': '*',
    // Accept: 'application/json',
    // 'Content-Type': 'application/json',
  }
};

const request = async (resource, method, data) => {
const url = `${host}/${resource}`;
  const response = await axios({
    url,
    data,
    method,
    defaultConfig,
  });
  // console.log(response);
  return response;
};

export const getDocsService = async () => {
  const response = await request('api/docs', "GET");
  return response.data
};

export const addDocService = async (data) => {
  const response = await request('api/add', "POST", data);
  return response.data
};

export const searchService = async (data) => {
  const response = await request('api/search', "POST", data);
  return response.data
};

