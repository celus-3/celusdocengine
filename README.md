# Celus Team 3


[Server] Python (Flask)

[Client] React

[Database] MongoDB

[Deployment] Docker, Yandex.Cloud

[Services] Yandex.Vision, Yandex.ObjectStorage

[Documentation] Swagger

## Getting Started

#### Install docker

You can use two variants.
The first one:
```
https://docs.docker.com/compose/install/
```
The second one:
```
brew install docker docker-machine docker-compose
docker-machine create --driver virtualbox Сelus
eval $(docker-machine env Сelus)
```

## Build and Run

```
git clone https://gitlab.com/celus-3/celusdocengine.git celus
cd celus/app
docker-compose up --build
frontend: http://localhost:3000
backend: http://localhost:5000/api
swagger http://localhost:5000/swagger
```


#### Note Docker

Enter in container:
```
docker exec -it flask bash
```
Remove containers:
```
docker-compose down
```
