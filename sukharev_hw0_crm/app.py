from flask import Flask
from manage_db import DB
from models import Models, products, customers
from flask import request
import json

app = Flask(__name__)

@app.route('/', methods=['GET'])
def hello_world():
    return 'Hello World!'


def get_db():
    db = DB()
    db.start_connection()
    return db


@app.route('/init', methods=['GET'])
def init():
    db = get_db()
    init_db(db)
    for i in range(len(products)):
        fill_db(db, i)
        db.connection.commit()
    db.close_connection()
    return "Tables created successfully in PostgreSQL"


def init_db(db):
    db.cursor.execute(Models.customers)
    db.cursor.execute(Models.products)
    db.cursor.execute(Models.transactions)
    db.cursor.execute(Models.transaction_details)
    db.connection.commit()
    print("Tables created successfully in PostgreSQL")


def fill_db(db, i):
    sql_customers = """INSERT INTO   customers (customer_name, customer_lastname, age, phone)
                         VALUES (%s, %s , %s, %s);"""
    sql_products = """INSERT INTO   products (title, description, price)
                         VALUES (%s, %s , %s);"""

    record_customers = (
        customers[i]['customer_name'], customers[i]['customer_lastname'], customers[i]['age'], customers[i]['phone'])
    record_products = (products[i]['title'], products[i]['description'], products[i]['price'])

    db.cursor.execute(sql_customers, record_customers)
    db.cursor.execute(sql_products, record_products)


@app.route('/sell', methods=['POST'])
def sell():
    db = get_db()
    req_params = request.json
    product_ids = req_params["product_ids"]
    customer_id = req_params["customer_id"]

    sql_transaction = "INSERT INTO transactions (customer_id) VALUES (%s) RETURNING transaction_id;"
    record_transaction = (customer_id,)
    transaction_id = db.base_write(sql_transaction, record_transaction)['transaction_id']
    print("id", transaction_id)

    sql_detail = "INSERT INTO transaction_details (transaction_id, product_id) VALUES (%s, %s) RETURNING transaction_detail_id;"
    for product_id in product_ids:
        record_transaction = (transaction_id, product_id)
        db.base_write(sql_detail, record_transaction)
        print("transaction detail is written")
    return {"transaction_id": transaction_id}


@app.route('/delete', methods=['POST'])
def delete():
    db = get_db()
    req_params = request.json
    customer_id = req_params['customer_id']
    sql = "UPDATE customers SET deleted_flg = '1' WHERE customer_id =%s RETURNING customer_id;"
    record = (customer_id,)
    db.base_write(sql, record)
    res = "user: {} is deleted".format(customer_id)
    return res


@app.route('/top', methods=['GET'])
def top():
    db = get_db()
    sql = """SELECT products.title, COUNT(transaction_details.product_id)
             FROM products, transaction_details
             WHERE products.product_id = transaction_details.product_id
             GROUP BY products.title
             LIMIT 10;
            ;"""
    res = db.base_get_all(sql)
    return json.dumps(res)


@app.route('/customers', methods=['GET'])
def get_customers():
    db = get_db()
    sql = "SELECT * FROM customers;"
    users = db.base_get_all(sql)
    print(users)
    return json.dumps(users)


@app.route('/products', methods=['GET'])
def get_products():
    db = get_db()
    sql = "SELECT * FROM products;"
    goods = db.base_get_all(sql)
    print(goods)
    return json.dumps(goods)


if __name__ == '__main__':
    host = '84.201.137.9'
    port = '3000'
    # app.run(host=host, port=port)
    app.run()

# python -m flask run --host=0.0.0.0
