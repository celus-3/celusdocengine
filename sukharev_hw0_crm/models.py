class Models:
    customers = '''
                CREATE TABLE IF NOT EXISTS customers(
                customer_id         SERIAL          NOT NULL PRIMARY KEY,
                customer_name       VARCHAR(64)     UNIQUE,
                customer_lastname   VARCHAR (64)    UNIQUE,
                age                 INT,
                phone               INT,
                deleted_flg         BOOLEAN DEFAULT '0'
                );'''

    products = '''
                    CREATE TABLE IF NOT EXISTS products(
                    product_id      SERIAL          NOT NULL PRIMARY KEY,
                    title           VARCHAR(1024)   UNIQUE,
                    description     TEXT,
                    price           INT
                    );'''

    transactions = '''
                    CREATE TABLE IF NOT EXISTS transactions(
                    transaction_id      SERIAL          NOT NULL PRIMARY KEY,
                    customer_id         INT REFERENCES customers (customer_id) ON DELETE CASCADE,
                    timestamp_at        TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
                    );'''

    transaction_details = '''
                    CREATE TABLE IF NOT EXISTS transaction_details(
                    transaction_detail_id SERIAL    NOT NULL PRIMARY KEY,
                    transaction_id                  INT REFERENCES transactions (transaction_id) ON DELETE CASCADE,
                    product_id                      INT REFERENCES products (product_id) ON DELETE CASCADE
                    );'''


customers = [
    {
        "customer_name": "oleg",
        "customer_lastname": "olegov",
        "age": 36,
        "phone": 123,
    },
    {
        "customer_name": "anton",
        "customer_lastname": "antonov",
        "age": 23,
        "phone": 111,
    },
]

products = [
    {
        "title": "apple",
        "description": "asdasdasd",
        "price": 164,
    },
    {
        "title": "milk",
        "description": "dsfsgfdggd",
        "price": 55,
    },
]
# **Customer** (id, name, lastname, age, phone, deleted_flg)
#
# **Transaction** (id, customer_id,  timestamp)
#
# **Transaction_details** (id, transaction_id, product_id)
#
# **Products** (id, title, description, price)
#
# где: deleted_flg - флаг логического удаления
