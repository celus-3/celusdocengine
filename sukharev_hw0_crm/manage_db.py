import psycopg2
import psycopg2.extras
from dotenv import load_dotenv
import os


class DB:

    def __init__(self):
        # self.DB_NAME = 'postgres'
        # self.DB_USER = 'postgres'
        # self.DB_PASS = 'postgres'
        # self.DB_HOST = 'postgres'
        load_dotenv()
        self.cursor = None
        self.connection = None
        self.DB_NAME = os.environ['POSTGRES_DB']
        self.DB_USER = os.environ['POSTGRES_USER']
        self.DB_PASS = os.environ['POSTGRES_PASSWORD']
        self.DB_HOST = os.environ['HOST']
        self.DB_PORT = os.environ['POSTGRES_PORT']

    def start_connection(self):
        try:
            print("start connection")
            connection = psycopg2.connect(dbname=self.DB_NAME,
                                          host=self.DB_HOST,
                                          port=self.DB_PORT,
                                          user=self.DB_USER,
                                          password=self.DB_PASS
                                          )
            cursor = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
            print("db is connected")
            self.connection = connection
            self.cursor = cursor

        except Exception as e:
            print("error start connect: ", e)
            return 0

    def close_connection(self):
        try:
            self.connection.commit()
            self.cursor.close()
            self.connection.close()
        except Exception as e:
            print("error close connect: ", e)

    def base_get_one(self, sql, record):
        try:
            self.start_connection()
            self.cursor.execute(sql, record)
            items = self.cursor.fetchone()
            self.close_connection()
            return items
        except Exception as e:
            print(e)

    def base_get_all(self, sql):
        try:
            self.start_connection()
            self.cursor.execute(sql)
            items = self.cursor.fetchall()
            self.close_connection()
            return items
        except Exception as e:
            print(e)

    def base_get_limited_all(self, sql, record):
        try:
            self.start_connection()
            self.cursor.execute(sql, record)
            items = self.cursor.fetchall()
            self.close_connection()
            return items
        except Exception as e:
            print(e)

    def base_write(self, sql, record):
        try:
            self.start_connection()
            self.cursor.execute(sql, record)
            id = self.cursor.fetchone()
            self.close_connection()
            return id
        except Exception as e:
            print(e)
            return "error"
